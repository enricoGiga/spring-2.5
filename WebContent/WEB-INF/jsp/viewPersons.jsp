<%@ page import="java.util.List" %>
<%@ page import="org.giga.Spring2.entity.Person" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>


<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<%@ page session="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>Person Page</title>


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${context}/styles/default.css">
    <script type="text/javascript" src="${context}/js/formjs.js"></script>

    <link rel="stylesheet" href="<%= request.getContextPath() %>/styles/default.css" type="text/css"/>

</head>
<%
    List<Person> personsModel = (List<Person>) request.getAttribute("personsModel");
%>
<body>
<form id="person" action="./persons" method="post">
    <table>

        <tbody>
        <tr>
            <td>
                <label for="name">
                    First Name
                </label>
            </td>
            <td>
                <input id="name" name="name" type="text" value="">
            </td>
            <td><span id="name-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <label for="surname">
                    Last Name
                </label>
            </td>
            <td>
                <input id="surname" name="surname" type="text" value="">
            </td>
            <td><span id="surname-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <label for="age">
                    Age
                </label>
            </td>
            <td>
                <input id="age" name="age" type="number" value="">
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">
                    E-mail
                </label>
            </td>
            <td>
                <input id="email" name="email" type="text" value="">
            </td>
            <td><span id="email-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <label for="country">
                    Country
                </label>
            </td>
            <td>
                <input id="country" name="country" type="text" value="">
            </td>
        </tr>
        <tr>
            <td colspan="2">


                <input type="submit" name="addPerson" value="Add Person">

            </td>
        </tr>
        </tbody>
    </table>
</form>


<h3>Persons List</h3>
<table class="formdata">
    <tr>
        <th width="80">ID</th>
        <th width="120">First Name</th>
        <th width="120">Last Name</th>
        <th width="80">Age</th>
        <th width="120">E-mail</th>
        <th width="120">Country</th>
        <th width="60">Edit</th>
        <th width="60">Delete</th>
    </tr>
    <%
        for (Person person : personsModel) {
    %>
    <tr>
        <td><%=person.getId()%>
        </td>
        <td><%=person.getName()%>
        </td>
        <td><%=person.getSurname()%>
        </td>
        <td><%=person.getAge()%>
        </td>
        <td><%=person.getEmail()%>
        </td>
        <td><%=person.getCountry()%>
        </td>
        <td><a href="./persons/edit?openEdit=<%=person.getId()%>">Edit</a></td>
        <td><a href="./persons?remove=<%=person.getId()%>">Delete</a></td>
    </tr>
    <% } %>

</table>


</body>
</html>
