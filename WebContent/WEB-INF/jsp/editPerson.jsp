<%@ page import="org.giga.Spring2.entity.Person" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
         
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}" /> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="${context}/styles/default.css">
<script type="text/javascript" src="${context}/js/formjs.js"></script>

<%
    Person personToEdit = (Person) request.getAttribute("personToEdit");
%>
</body>

<form id="person" action="<%=request.getContextPath()%>/persons/edit" method="post">
    <table>

        <tbody>
        <tr>
            <td>
                <label for="id">
                    Person ID
                </label>
            </td>
            <td>
                <input id="id" name="id" type="number" value="<%=personToEdit.getId()%>" readonly>
            </td>

        </tr>

        <tr>
            <td>
                <label for="name">
                    First Name
                </label>
            </td>
            <td>
                <input id="name" name="name" type="text" value="<%=personToEdit.getName()%>">
            </td>
            <td><span id="name-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <label for="surname">
                    Last Name
                </label>
            </td>
            <td>
                <input id="surname" name="surname" type="text" value="<%=personToEdit.getSurname()%>">
            </td>
            <td><span id="surname-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <label for="age">
                    Age
                </label>
            </td>
            <td>
                <input id="age" name="age" type="number" value="<%=personToEdit.getAge()%>">
            </td>
        </tr>
        <tr>
            <td>
                <label for="email">
                    E-mail
                </label>
            </td>
            <td>
                <input id="email" name="email" type="text" value="<%=personToEdit.getEmail()%>">
            </td>
            <td><span id="email-feedback"></span></td>
        </tr>
        <tr>
            <td>
                <label for="country">
                    Country
                </label>
            </td>
            <td>
                <input id="country" name="country" type="text" value="<%=personToEdit.getCountry()%>">
            </td>
        </tr>
        <tr>
            <td colspan="2">


                <input type="submit" name="editPerson" value="Edit Person">

            </td>
        </tr>
        </tbody>
    </table>
</form>
</html>