<%@ page import="java.util.Map" %>
<%@ page import="org.giga.Spring2.zipReader.ZipNode" %>
<%@ page import="java.util.Set" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page buffer="64kb" %>
<%
    Map<String, Object> jsonMap = (Map<String, Object>) request.getAttribute("jsonMap");
    Set<ZipNode> tableJson = (Set<ZipNode>) jsonMap.get("tableJson");
    String jsTreeJson = (String) jsonMap.get("jsTreeJson");
%>
<!DOCTYPE html>
<html>
    <head>
        <title>jsTree test</title>
        <!-- 2 load the theme CSS file -->
        <%--        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"/>--%>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/themes/default/style.min.css"/>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/themes/default/zipfile.css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
                crossorigin="anonymous"></script>

    </head>
    <body>
        <div class="row">
            <div class="col-12" style="height: 36px">

            </div>
        </div>


        <div class="content page">
            <div class="row">
                <div class="col-md-3">
                    <div id="using_json" class="tree"></div>
                </div>
                <div id="table_id" class="col-md-9">
                    <div class="green">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">File name</th>
                                    <th scope="col">Creation Time</th>
                                    <th scope="col">Last Modified Time</th>
                                    <th scope="col">Size</th>
                                    <th scope="col">Compressed Size</th>
                                    <th scope="col" title="download zip">
                                        <a href="<%= request.getContextPath() %>/zipfile?action=DOWNLOAD_ZIP"><i
                                                id="download-zip" class="bi bi-download"></i></a>
                                    </th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="par" items="<%=tableJson%>">
                                <c:if test="${!par.directory}">
                                <tr class="riga" id="<c:out value="${par.id}table"/>"
                                    data-arg="<c:out value="${par.text}"/>">
                                    <td><c:out value="${par.text}"/></td>
                                    <td><c:out value="${par.creationTime}"/></td>
                                    <td><c:out value="${par.lastModifiedTime}"/></td>
                                    <td><c:out value="${par.size}"/></td>
                                    <td><c:out value="${par.compressedSize}"/></td>
                                    <td style="cursor: pointer" title="mostra file"><i
                                            id="<c:out value="${par.id}pdf"/>"
                                            class="bi bi-eye"
                                            data-arg="<c:out value="${par.name}"/>"
                                    ></i></td>

                                </tr>
                                </c:if>

                                </c:forEach>
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <meta charset="utf-8">
        <script src="dist/libs/jquery-3.5.1.min.js"></script>

        <script src="dist/jstree.min.js"></script>

        <script>

            $(function () {

                const $usingJson = $('#using_json');
                const jsTree = JSON.parse('<%=jsTreeJson%>');

                $usingJson.jstree({
                    'core': {
                        'data': jsTree
                    },
                    'plugins': ['theme', 'html_data', 'types']
                });
                var tableWidth = $("#table_id").height();
                $("#using_json").css("height", tableWidth);


                $usingJson.on("changed.jstree", function (e, data) {
                    const node = data.node.original;

                    const id = node.id + "table";
                    $("#table_id tr").removeClass("table-info");
                    $('#' + id).addClass("table-info");
                });

                $('.riga td i').on("click", function (event) {
                    const id = event.target.id;
                    const arg = $('#' + id).attr('data-arg');
                    const url = "zipfile?action=STATIC_DOWNLOAD&filePath=" + arg;
                    window.open(url);
                });

                // $('#download-zip').on("click", function (event) {
                //
                //     const url = "zipfile?action=DOWNLOAD_ZIP";
                //
                // });
            });
        </script>
    </body>
</html>