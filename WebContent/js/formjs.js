/**
 * 
 */


$(function () {
$("#person").submit(function (event) {


				const firstName = $("#name").val();

				const lastName = $("#surname").val();

				const age = $("#age").val();

				const email = $("#email").val();

				const country = $("#country").val();


				validateNameField(firstName, event);
				validateNameField(lastName, event);
				validateEmailField(email, event);

				function validateNameField(name, event) {
					highlight($(this), isValidName(name));
					if (!isValidName(name)) {
						event.preventDefault();
						$("#name-feedback").text("Please enter at least two characters");
					}
				}
				function validateEmailField(email, event) {
					highlight($(this), isValidName(email));
					if (!validateEmail(email)) {
						event.preventDefault();
						$("#email-feedback").text("Please enter a valid email");
					}
				}
			});

			function isValidName(name) {
				return name.trim().length >= 2;
			}
			function validateEmail($email) {
				const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				return emailReg.test( $email );
			}


			enableFastFeedback($("#person"));

			function enableFastFeedback(formElement) {
				const firstName = formElement.find("#name");
				const lastName = formElement.find("#surname");
				const email = formElement.find("#email");


				firstName.blur(function () {
					const name = $(this).val();
					highlight($(this), isValidName(name));
					if (!isValidName(name)) {
						$("#name-feedback").text("Please enter at least two characters");
					} else {
						$("#name-feedback").text("");
					}
				});

				lastName.blur(function () {
					const name = $(this).val();
					highlight($(this), isValidName(name));
					if (!isValidName(name)) {
						$("#surname-feedback").text("Please enter at least two characters");
					} else {
						$("#surname-feedback").text("");
					}
				});
				email.blur(function () {
					const email = $(this).val();
					highlight($(this), validateEmail(email));
					if (!validateEmail(email)) {
						$("#email-feedback").text("Please enter a valid email");
					} else {
						$("#email-feedback").text("");
					}
				})

			}

			function highlight(element, isValid) {
				let color = "#811";  // red
				if (isValid) {
					color = "#181";  // green
				}

				element.css("box-shadow", "0 0 4px " + color);
			}
	
	

});