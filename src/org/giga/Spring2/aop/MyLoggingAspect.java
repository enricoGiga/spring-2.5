package org.giga.Spring2.aop;


import org.aspectj.lang.JoinPoint;

public class MyLoggingAspect
{
    public Object before(JoinPoint call) throws Throwable
    {

//((MethodInvocationProceedingJoinPoint) call).proceed()
        Object point =  call.getThis();

        System.out.println("-----BEFORE-----: from logging aspect: exiting method [" + call.toShortString()
                + "with return as:" +point);

        return point;
    }
    public Object after(JoinPoint call) throws Throwable
    {

//((MethodInvocationProceedingJoinPoint) call).proceed()
        Object point =  call.getThis();

        System.out.println("-----AFTER-----: from logging aspect: exiting method [" + call.toShortString()
                + "with return as:" +point);

        return point;
    }
}