package org.giga.Spring2.zipReader;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.net.URLConnection;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.springframework.util.FileCopyUtils.BUFFER_SIZE;

public class ZipFileControllerHelper {


    public static ZipFileControllerAction fetchCurrentAction(HttpServletRequest request) {
        ZipFileControllerAction action;
        String actionParam = request.getParameter("action");
        if (actionParam == null) {
            actionParam = "DEFAULT";
        }
        action = ZipFileControllerAction.valueOf(actionParam);
        return action;
    }

    public static void downloadFile(HttpServletRequest request, HttpServletResponse response, ZipFile zipFile) {
        String filePath = request.getParameter("filePath");
        try {
            ZipEntry zipEntry = zipFile.getEntry(filePath);
            InputStream is = zipFile.getInputStream(zipEntry);
            org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();

        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }


    }

    public static void doDownload(HttpServletRequest request,
                           HttpServletResponse response) throws IOException {
        System.out.println(request.getContextPath());
        // construct the complete absolute path of the file
        File downloadFile = new File("C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip");
        String mimeType = URLConnection.guessContentTypeFromName(downloadFile.getName());
        FileInputStream inputStream = new FileInputStream(downloadFile);

        // get MIME type of the file

        if (mimeType == null) {
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        System.out.println("MIME type: " + mimeType);

        // set content attributes for the response
        response.setContentType(mimeType);
        response.setContentLength((int) downloadFile.length());

        // set headers for the response
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"",
                downloadFile.getName());
        response.setHeader(headerKey, headerValue);

        // get output stream of the response
        OutputStream outStream = response.getOutputStream();

        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;

        // write bytes read from the input stream into the output stream
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, bytesRead);
        }

        inputStream.close();
        outStream.close();

    }
}
