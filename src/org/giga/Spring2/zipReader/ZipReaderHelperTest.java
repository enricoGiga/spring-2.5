package org.giga.Spring2.zipReader;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ZipReaderHelperTest {
    static ZipFile zipFile = null;
    static String filePath = null;

    @BeforeEach
    void before() throws IOException {
        zipFile = new ZipFile("C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip");
        filePath = "C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip";
    }

    @Test
    public void testOpenZip() {

        try {


            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();

                if (entry.isDirectory()) {
                    System.out.println("dir  : " + entry.getName());
                } else {
                    System.out.println("file : " + entry.getName());
                }
                InputStream stream = zipFile.getInputStream(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldCreateTreeObjectFromZip() throws IOException {

        ZipFile file = zipFile;
        Assertions.assertDoesNotThrow(() -> new ZipFileReader(file).process());

        file.close();


    }

    @Test
    public void shouldSerializeTreeObjectToJson() throws IOException {
        ZipFile file = zipFile;
        ZipNode root = new ZipFileReader(file).process();
        file.close();
        ObjectMapper objectMapper = new ObjectMapper();


        Assertions.assertDoesNotThrow(() -> objectMapper.writeValueAsString(root));

        System.out.println(objectMapper.writeValueAsString(root));
    }
    @Test
    public void shouldGetAllLeaf() throws IOException {
        ZipFile file = zipFile;
        ZipNode root = new ZipFileReader(file).process();
        Set<ZipNode> allLeafNodes = ZipHelper.getAllLeafNodes(root);
        ObjectMapper objectMapper = new ObjectMapper();

        file.close();
        System.out.println(objectMapper.writeValueAsString(allLeafNodes));
    }

    @Test
    public void whenUsingGuessContentTypeFromName_thenSuccess(){
        File file = new File("C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip");
        String mimeType = URLConnection.guessContentTypeFromName(file.getName());

        assertEquals(mimeType, "application/zip");
    }
    @Test
    public void shouldGetThePath() {
        String value = "\\";
        String string = "C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip";
        int i = string.lastIndexOf(value);
        String substring = string.substring(0, i+1);
        assertEquals(substring, "C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\");
    }
}