package org.giga.Spring2.zipReader;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * It helps creating a tree of ZipNodes from a ZipFile.
 */
public class ZipFileReader {
    /**
     * The file to be read.
     */
    private final ZipFile file;

    /**
     * The nodes collected so far.
     */
    private final Map<String, ZipNode> collected;

    /**
     * our root node.
     */
    private final ZipNode root;

    /**
     * creates a new ZipFileReader from a ZipFile.
     */
    public ZipFileReader(ZipFile f) {
        this.file = f;
        this.collected = new HashMap<>();
        collected.put("", null);
        root = new ZipNode(null, f.getName());
    }

    /**
     * reads all entries, creates the corresponding Nodes and
     * returns the root node.
     */
    public ZipNode process() {
        for (Enumeration<? extends ZipEntry> entries = file.entries();
             entries.hasMoreElements(); ) {
            this.addEntry(entries.nextElement());
        }
        return root;
    }

    /**
     * adds an entry to our tree.
     * <p>
     * This may create a new ZipNode and then connects
     * it to its parent node.
     *
     * @return the ZipNode corresponding to the entry.
     */
    private ZipNode addEntry(ZipEntry entry) {
        String name = entry.getName();
        ZipNode node = collected.get(name);
        if (node != null) {
            // already in the map
            return node;
        }
        node = new ZipNode(entry, file.getName());
        collected.put(name, node);
        this.findParent(node);
        return node;
    }

    /**
     * makes sure that the parent of a
     * node is in the collected-list as well, and this node is
     * registered as a child of it.
     * If necessary, the parent node is first created
     * and added to the tree.
     */
    private void findParent(ZipNode node) {
        String nodeName = node.getEntry().getName();
        int slashIndex = nodeName.lastIndexOf('/', nodeName.length() - 2);
        if (slashIndex < 0) {
            // top-level-node
            connectParent(root, node, nodeName);
            return;
        }
        String parentName = nodeName.substring(0, slashIndex + 1);
        ZipNode parent = addEntry(file.getEntry(parentName));
        connectParent(parent, node, nodeName.substring(slashIndex + 1));
    }

    /**
     * connects a parent node with its child node.
     */
    private void connectParent(ZipNode parent, ZipNode child,
                               String childName) {
        child.setParent(parent);
        parent.getChildren().put(childName, child);
    }


}