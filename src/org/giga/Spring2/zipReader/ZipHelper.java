package org.giga.Spring2.zipReader;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.zip.ZipFile;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZipHelper {

    public static String getLastComponentName(String fullName, String separator) {
        return fullName.substring(fullName.lastIndexOf(separator,
                fullName.length() - 2) + 1);
    }

    public static String stringFromInstant(Instant instant) {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
                        .withLocale(Locale.ITALY)
                        .withZone(ZoneId.systemDefault());
        return formatter.format(instant);
    }

    public static TreeSet<ZipNode> getAllLeafNodes(ZipNode root) {
        TreeSet<ZipNode> leafNodes = new TreeSet<>((o1, o2) -> o1.getName().compareTo(o2.getName()));

        if (root.getChildren().isEmpty()) {
            leafNodes.add(root);
        } else {
            for (ZipNode child : root.getChildren().values()) {
                leafNodes.addAll(getAllLeafNodes(child));
            }
        }
        return leafNodes;
    }

    public static String createJsonFromZipFile(ZipNode zipNode) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(zipNode);
    }

    public static String getLeavesFromNode(ZipNode zipNode) throws JsonProcessingException {
        Set<ZipNode> allLeafNodes = ZipHelper.getAllLeafNodes(zipNode);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(allLeafNodes);
    }


    public static String getThePathOfTheZip(String fullPath, String separator) {
        int i = fullPath.lastIndexOf(separator);
        return fullPath.substring(0, i + 1);
    }


}
