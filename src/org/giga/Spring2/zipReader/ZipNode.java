package org.giga.Spring2.zipReader;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipEntry;


/**
 * A immutable wrapper around {@link ZipEntry} allowing
 * simple access of the children of directory entries.
 * Please see the {@link org.giga.Spring2.zipReader.ZipNode}
 *
 * @author Enrico Gigante
 */
@JsonIdentityInfo(
        generator = ObjectIdGenerators.None.class,
        property = "id")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ZipNode {

    private String id;
    @JsonIgnore
    private ZipNode parent;
    @JsonSerialize(using = RemoveKeyFromMap.class)
    private final Map<String, ZipNode> children = new LinkedHashMap<>();
    private long size;

    /**
     * the full name with the zip path
     */
    private String name;
    private boolean isDirectory;
    private String lastModifiedTime;
    private String creationTime;
    private long compressedSize;

    /**
     * the fields linked down are required from JsTree to populate the tree using json
     * {@link ZipNode#text} {@link ZipNode#icon} {@link ZipNode#state}
     * @see <a href="https://www.jstree.com/docs/json/ ">jstree with json</a>
     */
    private String text;
    private String icon;
    private JsTreeNodeState state;

    /**
     * the corresponding Zip entry. If null, this is the root entry.
     */
    @JsonIgnore
    private final ZipEntry entry;

    /**
     * Initialize the zip node
     *
     * @param entry   the zip entry node
     * @param zipName the zip name, full path
     */
    public ZipNode(ZipEntry entry, String zipName) {

        this.entry = entry;
        fillNodeData();
        fillJsTreeData(zipName);
    }

    /**
     * This fields are required by JsTree framework
     *
     * @param zipName the zip name, full path
     */
    private void fillJsTreeData(String zipName) {
        state = new JsTreeNodeState();
        if (this.entry == null) {
            this.icon = "dist/themes/default/zip-icon.png";
            this.text = ZipHelper.getLastComponentName(zipName, "\\");
        } else {
            text = ZipHelper.getLastComponentName(entry.getName(), "/");
            if (!entry.isDirectory()) {
                if (this.name.endsWith(".jpg")) {
                    icon = "dist/themes/default/Files-Jpg-icon.png";
                } else {
                    icon = "dist/themes/default/pdf-16.png";
                }

            } else {
                icon = "dist/themes/default/Folder-icon.png";
            }
        }
    }

    /**
     * Fill data with the entry data, if the entry is null this is the root node
     */
    private void fillNodeData() {
        this.id = UUID.randomUUID().toString();
        if (this.entry == null) {
            this.isDirectory = true;
            this.name = "";
            this.size = 0;
            this.compressedSize = 0;
            this.lastModifiedTime = "";
            this.creationTime = "";
        } else {
            this.size = entry.getSize();
            this.name = entry.getName();
            this.isDirectory = entry.isDirectory();
            this.size = entry.getSize();
            this.compressedSize = entry.getCompressedSize();
            this.lastModifiedTime = ZipHelper.stringFromInstant(entry.getLastModifiedTime().toInstant());
            this.creationTime = ZipHelper.stringFromInstant(entry.getCreationTime().toInstant());
        }

    }


    /**
     * gets the corresponding ZipEntry to this node.
     *
     * @return {@code null} if this is the root node (which has no
     * corresponding entry), else the corresponding ZipEntry.
     */
    public ZipEntry getEntry() {
        return entry;
    }

    /**
     * returns this node's parent node (null, if this is the root node).
     */
    public ZipNode getParent() {
        return parent;
    }

    public void setParent(ZipNode parent) {
        this.parent = parent;
    }

    public Map<String, ZipNode> getChildren() {
        return children;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean directory) {
        isDirectory = directory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public long getCompressedSize() {
        return compressedSize;
    }

    public void setCompressedSize(long compressedSize) {
        this.compressedSize = compressedSize;
    }

    public JsTreeNodeState getState() {
        return state;
    }

    public void setState(JsTreeNodeState state) {
        this.state = state;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
