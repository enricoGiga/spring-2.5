package org.giga.Spring2.zipReader;

public class JsTreeNodeState {


    private boolean opened;
    private boolean disabled;
    private boolean selected;

    public JsTreeNodeState() {
        this.opened = true;
        this.disabled = false;
        this.selected = false;
    }

    public JsTreeNodeState(boolean opened, boolean disabled, boolean selected) {
        this.opened = opened;
        this.disabled = disabled;
        this.selected = selected;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
