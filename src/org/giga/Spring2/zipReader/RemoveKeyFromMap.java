package org.giga.Spring2.zipReader;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Map;
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RemoveKeyFromMap extends JsonSerializer<Map<?, ?>> {
    @Override
    public void serialize(Map<?, ?> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeObject(value.values());

    }
}
