package org.giga.Spring2.zipReader;

public enum ZipFileControllerAction {


    STATIC_DOWNLOAD,
    DEFAULT,
    DOWNLOAD_ZIP
}
