package org.giga.Spring2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.giga.Spring2.entity.Person;
import org.giga.Spring2.service.PersonService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class PersonController implements Controller {

	private PersonService service;

	public void setPersonService(PersonService service) {
		this.service = service;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String dest = "viewPersons";
		String addPerson = request.getParameter("addPerson");
		String removeId = request.getParameter("remove");
		if (addPerson != null && addPerson.equals("Add Person")) {
			Person person = new Person();
			person.setName(request.getParameter("name"));
			person.setSurname(request.getParameter("surname"));
			person.setAge(Integer.parseInt(request.getParameter("age")));
			person.setCountry(request.getParameter("country"));
			person.setEmail(request.getParameter("email"));
			service.save(person);
		}
		if (removeId != null) {
			service.delete(Long.parseLong(removeId, 10));
		}

		return new ModelAndView(dest, "personsModel", service.findAll());
	}
}
