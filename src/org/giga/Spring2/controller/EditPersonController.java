package org.giga.Spring2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.giga.Spring2.entity.Person;
import org.giga.Spring2.service.PersonService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

public class EditPersonController implements Controller {
	private PersonService personService;

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String editId = request.getParameter("openEdit");
		String editPerson = request.getParameter("editPerson");

		if (editId != null && editPerson == null) {
			Person person = personService.findById(Long.parseLong(editId, 10));
			request.removeAttribute(editId);
			return new ModelAndView("editPerson", "personToEdit", person);
		} else {
			Person person = new Person();
			person.setId(Long.parseLong(request.getParameter("id"), 10));
			person.setAge(Integer.parseInt(request.getParameter("age")));
			person.setCountry(request.getParameter("country"));
			person.setEmail(request.getParameter("email"));
			person.setName(request.getParameter("name"));
			person.setSurname(request.getParameter("surname"));
			RedirectView redirectView = new RedirectView(request.getContextPath() + "/persons");
			personService.updatePerson(person);
			return new ModelAndView(redirectView);
		}

	}
}
