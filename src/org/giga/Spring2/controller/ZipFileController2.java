package org.giga.Spring2.controller;

import org.giga.Spring2.zipReader.ZipFileControllerAction;
import org.giga.Spring2.zipReader.ZipFileReader;
import org.giga.Spring2.zipReader.ZipHelper;
import org.giga.Spring2.zipReader.ZipNode;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipFile;

import static org.giga.Spring2.zipReader.ZipFileControllerHelper.*;

public class ZipFileController2 implements Controller {

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ZipFileControllerAction action;
        try (ZipFile zipFile = new ZipFile("C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip")) {
            action = fetchCurrentAction(request);

            switch (action) {
                case STATIC_DOWNLOAD:
                    downloadFile(request, response, zipFile);
                    return null;
                case DEFAULT:
                    ZipNode root = new ZipFileReader(zipFile).process();
                    TreeSet<ZipNode> allLeafNodes = ZipHelper.getAllLeafNodes(root);
                    Set<ZipNode> allLeafNodesModified = allLeafNodes.stream()
                            .peek(zipNode -> zipNode.setName(ZipHelper.getThePathOfTheZip(zipNode.getName(), "/")))
                            .sorted(Comparator.comparing(ZipNode::getName))
                            .collect(Collectors.toCollection(LinkedHashSet::new));

                    Map<String, Object> map = new HashMap<>();

                    map.put("tableJson", allLeafNodesModified);
                    return new ModelAndView("zipFile2", "jsonMap", map);
                case DOWNLOAD_ZIP:
                    doDownload(request, response);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


}
