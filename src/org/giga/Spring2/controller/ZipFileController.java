package org.giga.Spring2.controller;

import org.giga.Spring2.zipReader.ZipFileControllerAction;
import org.giga.Spring2.zipReader.ZipFileReader;
import org.giga.Spring2.zipReader.ZipHelper;
import org.giga.Spring2.zipReader.ZipNode;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.zip.ZipFile;

import static org.giga.Spring2.zipReader.ZipFileControllerHelper.*;

public class ZipFileController implements Controller {

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ZipFileControllerAction action;
        try (ZipFile zipFile = new ZipFile("C:\\Users\\GIGENR\\Desktop\\DOCZUCCHETTI\\rischio basso.zip")) {
            action = fetchCurrentAction(request);

            switch (action) {
                case STATIC_DOWNLOAD:
                    downloadFile(request, response, zipFile);
                    return null;
                case DEFAULT:
                    ZipNode root = new ZipFileReader(zipFile).process();
                    String jsTreeJson = ZipHelper.createJsonFromZipFile(root);
                    TreeSet<ZipNode> allLeafNodes = ZipHelper.getAllLeafNodes(root);
                    Map<String, Object> map = new HashMap<>();
                    map.put("jsTreeJson", jsTreeJson);
                    map.put("tableJson", allLeafNodes);
                    return new ModelAndView("zipFile", "jsonMap", map);
                case DOWNLOAD_ZIP:
                    doDownload(request, response);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }





}
