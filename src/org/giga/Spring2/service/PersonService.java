package org.giga.Spring2.service;

import org.giga.Spring2.entity.Person;

import java.util.List;

public interface PersonService {
    void save(Person entity);
    void updatePerson(Person person);
    void delete(Long person);

    List<Person> findAll();
    Person findById(Long id);


}
