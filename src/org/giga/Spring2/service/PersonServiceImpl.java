package org.giga.Spring2.service;


import org.giga.Spring2.dao.PersonDAO;
import org.giga.Spring2.entity.Person;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor=Throwable.class)
public class PersonServiceImpl implements PersonService {

    private PersonDAO personDao;


    public void setPersonDao(PersonDAO personDao) {
        this.personDao = personDao;
    }

    @Override
    public void save(Person entity) {
        try {
            personDao.save(entity);
        } catch (Throwable throwable) {
            System.out.println("error on save");
            System.out.println(throwable.getLocalizedMessage());
        }
    }

    @Override
    public List<Person> findAll() {
        return personDao.findAll();
    }

    @Override
    public Person findById(Long id) {
        return personDao.findById(id);
    }

    @Override
    public void updatePerson(Person p) {
        personDao.updatePerson(p);
    }

    @Override
    public void delete(Long id) {
        this.personDao.delete(id);
    }
}
