package org.giga.Spring2.service;

import java.io.Serializable;

public interface OperationService extends Serializable{

	public int makeSum(int x, int y);
}
