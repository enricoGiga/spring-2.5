package org.giga.Spring2.dao;

import org.giga.Spring2.entity.Person;

import java.io.Serializable;
import java.util.List;

public interface PersonDAO {
	 void updatePerson(Person p);
	 void save(Person entity);
	 void delete(Long p);
	 List<Person> findAll();
	 Person findById(Long id);
}
