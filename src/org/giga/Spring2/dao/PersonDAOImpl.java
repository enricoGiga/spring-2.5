package org.giga.Spring2.dao;

import org.giga.Spring2.entity.Person;
import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor=Throwable.class)
public class PersonDAOImpl extends HibernateDaoSupport implements PersonDAO  {



    @Override
    public void updatePerson(Person p) {
        getSession().update(p);

    }

    @Override
    public void save(Person entity) {
        getHibernateTemplate().persist(entity);
    }

    public List<Person> findAll() {
        Query query = getSession().createQuery("FROM Person");

        return query.list();
    }

    @Override
    public Person findById(Long id) {
        return (Person) getSession().get(Person.class, id);
    }

    @Override
    public void delete(Long id) {
        Person ent = (Person) getSession().get(Person.class, id);
        getSession().delete(ent);


    }
}
